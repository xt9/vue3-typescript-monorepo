module.exports = {
  root: true,
  parserOptions: {
    parser: '@typescript-eslint/parser',
    project: './tsconfig.eslint.json',
    ecmaVersion: 2021,
    extraFileExtensions: [
      '.vue',
    ],
  },
  plugins: [
    '@typescript-eslint',
    'vue',
  ],
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    '@vue/typescript/recommended',
  ],
  rules: {
    'no-plusplus': 'off',
    'no-use-before-define': 'off',
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],
    'import/extensions': 'off',
    'import/prefer-default-export': 'off',
    'max-len': 'off',
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'no-param-reassign': ['error', { props: false }],
    '@typescript-eslint/strict-boolean-expressions': ['error', { allowString: false, allowNumber: false }],
    '@typescript-eslint/explicit-function-return-type': 'error',
  },
  ignorePatterns: [
    'dist',
    'node_modules',
  ],
};
