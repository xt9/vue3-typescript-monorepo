import { add } from '@xt9/shared';
import Validators from '@xt9/shared/src/validators';

export interface User {
  name: string;
  email: string;
  type: UserType;
  age: number;
}

export enum UserType {
  TEACHER = 'TEACHER',
  SUPERVISOR = 'SUPERVISOR'
}

export function createUser(name: string | number | boolean, email: string | number | boolean): User {
  if (!Validators.validateString(name)) {
    throw Error('not a string');
  }

  if (!Validators.validateString(email)) {
    throw Error('not a string');
  }

  return {
    name,
    email,
    type: UserType.SUPERVISOR,
    age: add(1, 22),
  };
}

export function getUsers(): User[] {
  const result: User[] = [];
  result.push(createUser('Niklas', 'niklas@wematter.se'));
  return result;
}
