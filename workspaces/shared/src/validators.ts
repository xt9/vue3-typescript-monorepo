export default class Validators {
  public static validateString(value: string | boolean | number): value is string {
    return typeof value === 'string';
  }
}
