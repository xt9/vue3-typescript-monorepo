// eslint-disable-next-line import/no-extraneous-dependencies
import { UserConfig } from 'vite';

const config: UserConfig = {
  optimizeDeps: {
    exclude: [
      '@xt9/backend',
      '@xt9/shared',
    ],
  },
};

export default config;
