import { getUsers, User, UserType } from '@xt9/backend';
import { add } from '@xt9/shared';

export async function fetchUsers(): Promise<User[]> {
  return getUsers();
}

export async function updateUser(user: User): Promise<User> {
  const newAge = add(12, 22);
  user.age = newAge;
  return user;
}

export function isUserAdmin(user: User): boolean {
  return user.type === UserType.SUPERVISOR;
}
